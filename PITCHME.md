# FluxTEST 

An application architecture for React

---

### Flux Design

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React
- $$\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}$$

---

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)